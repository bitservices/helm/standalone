{{/* vim: set filetype=mustache: */}}
{{/* --------------------------------------------------------------------- */}}
{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "standalone.chart" -}}
  {{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{/* --------------------------------------------------------------------- */}}
{{/*
Generate standard set of labels to be used throughout the chart.
*/}}
{{- define "standalone.labels" -}}
{{- range $k, $v := .Values.global.labels }}
{{ $k }}: "{{ tpl $v $ }}"
{{- end -}}
{{- end -}}
{{/* ---------------------------------------------------------------------- */}}
{{/*
Generate web selector to be used throughout the chart.
*/}}
{{- define "standalone.web.selector" -}}
{{- range $k, $v := merge .Values.web.selector .Values.global.selector }}
{{ $k }}: "{{ tpl $v $ }}"
{{- end -}}
{{- end -}}
{{/* ---------------------------------------------------------------------- */}}
{{/*
Generate web labels to be used throughout the chart.
*/}}
{{- define "standalone.web.labels" -}}
{{- range $k, $v := merge .Values.web.labels .Values.global.labels }}
{{ $k }}: "{{ tpl $v $ }}"
{{- end -}}
{{- end -}}
{{/* ---------------------------------------------------------------------- */}}
{{/*
Generate web affinity to be used throughout the chart.
*/}}
{{- define "standalone.web.affinity" -}}
{{- range $k, $v := merge .Values.web.labels .Values.global.labels }}
- key: "{{ $k }}"
  operator: In
  values:
    - "{{ tpl $v $ }}"
{{- end -}}
{{- end -}}
{{/* ---------------------------------------------------------------------- */}}
{{/*
Generate cron labels to be used throughout the chart.
*/}}
{{- define "standalone.cron.labels" -}}
{{- range $k, $v := merge .Values.cron.labels .Values.global.labels }}
{{ $k }}: "{{ tpl $v $ }}"
{{- end -}}
{{- end -}}
{{/* ---------------------------------------------------------------------- */}}
{{/*
Generate processing selector to be used throughout the chart.
*/}}
{{- define "standalone.processing.selector" -}}
{{- range $k, $v := merge .Values.processing.selector .Values.global.selector }}
{{ $k }}: "{{ tpl $v $ }}"
{{- end -}}
{{- end -}}
{{/* ---------------------------------------------------------------------- */}}
{{/*
Generate processing labels to be used throughout the chart.
*/}}
{{- define "standalone.processing.labels" -}}
{{- range $k, $v := merge .Values.processing.labels .Values.global.labels }}
{{ $k }}: "{{ tpl $v $ }}"
{{- end -}}
{{- end -}}
{{/* ---------------------------------------------------------------------- */}}
{{/*
Generate processing affinity to be used throughout the chart.
*/}}
{{- define "standalone.processing.affinity" -}}
{{- range $k, $v := merge .Values.processing.labels .Values.global.labels }}
- key: "{{ $k }}"
  operator: In
  values:
    - "{{ tpl $v $ }}"
{{- end -}}
{{- end -}}
{{/* ---------------------------------------------------------------------- */}}
{{/*
Generate migration labels to be used throughout the chart.
*/}}
{{- define "standalone.migration.labels" -}}
{{- range $k, $v := merge .Values.migration.labels .Values.global.labels }}
{{ $k }}: "{{ tpl $v $ }}"
{{- end -}}
{{- end -}}
{{/* ---------------------------------------------------------------------- */}}
